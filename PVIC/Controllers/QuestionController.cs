﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PVIC.Models;
using Microsoft.AspNet.Identity;
using System.Net.Mail;

namespace PVIC.Controllers
{
    public class QuestionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Question
        public ActionResult Index()
        {
            var questions = db.Questions.Include(q => q.CreatedByUser);
            return View(questions.ToList());
        }

        // GET: Question/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // GET: Question/Create
        [Authorize]
        public ActionResult Create()
        {
            //ViewBag.CreatedById = new SelectList(db.ApplicationUsers, "Id", "FirstName");
            return View();
        }

        // POST: Question/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "QuestionPost")] Question question)
        {
            if (ModelState.IsValid)
            {
                question.CreatedById = this.User.Identity.GetUserId();
                question.PostDate = DateTime.Now;

                db.Questions.Add(question);
                db.SaveChanges();

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = "smtp.mail.yahoo.com";
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("bill-vickers@att.net", "GavinLucas1");
                mail.To.Add(new MailAddress("bill-vickers@att.net"));
                mail.From = new MailAddress("househunter.realty@att.net");
                mail.Subject = "Something was posted";
                mail.Body = "You have received a post in PonteVedraClub.com.";
                client.Send(mail);               

                return RedirectToAction("Index", "Home");
            }

            //ViewBag.CreatedById = new SelectList(db.ApplicationUsers, "Id", "FirstName", question.CreatedById);
            return View(question);
        }

        // GET: Question/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            //ViewBag.CreatedById = new SelectList(db.ApplicationUsers, "Id", "FirstName", question.CreatedById);
            return View(question);
        }

        // POST: Question/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "QuestionId,QuestionPost,PostDate,CreatedById")] Question question)
        {
            if (ModelState.IsValid)
            {
                db.Entry(question).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.CreatedById = new SelectList(db.ApplicationUsers, "Id", "FirstName", question.CreatedById);
            return View(question);
        }

        // GET: Question/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // POST: Question/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Question question = db.Questions.Find(id);
            db.Questions.Remove(question);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
