﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PVIC.Models
{
    public class Question
    {
        [Key]
        public int QuestionId { get; set; }
        [Required(ErrorMessage = "Your Post is required")]
        [DataType(DataType.MultilineText)]
        [StringLength(1000, ErrorMessage = "Your country can not be more than 1000 characters long")]
        [DisplayName("Enter Your Question")]
        public string QuestionPost { get; set; }
        public DateTime PostDate { get; set; }

        [ForeignKey("CreatedByUser")]
        public virtual string CreatedById { get; set; }
        public ApplicationUser CreatedByUser { get; set; }
    }
}
