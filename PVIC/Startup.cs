﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PVIC.Startup))]
namespace PVIC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
